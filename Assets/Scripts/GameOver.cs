﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public GameObject GameoverUI;
    public Animator myAnimator;
    //public static bool GameIsOver = false;
    public void LoadDeath()
    {
        SceneManager.LoadScene("Intro_scene");
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("tag:"+other);
        if (other.tag == "obstaculo")
        {
            Invoke("LoadDeath", 0.0f);
            GameoverUI.SetActive(true);
            Time.timeScale = 1;
            //GameIsOver = true;


            //myAnimator.SetTrigger("choque");
        }
    }

    public void OnFinishDyingAnimation()
    {
        GameoverUI.SetActive(true);
        Time.timeScale = 0;
        //PauseMenu.IsPause = true;
    }
}