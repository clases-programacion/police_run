﻿
using System.Collections;
using System.Collections.Generic;


using UnityEngine;

public class spawner : MonoBehaviour
{

    public GameObject spawnee;
    public bool stopspawning = false;
    public float spawnTime;
    public float spawnDelay;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    // Update is called once per frame
    public void SpawnObject()
    {   
        Debug.Log(Random.Range(0, 3));
        Instantiate(spawnee, transform.position, transform.rotation);
        if (stopspawning)
        {
            CancelInvoke("SpawnObject");
        }
    }
}
