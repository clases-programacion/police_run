﻿
using System.Collections;
using System.Collections.Generic;


using UnityEngine;

public class spawner_2 : MonoBehaviour
{

    public GameObject spawnee;
    public bool stopspawning = false;
    public float spawnTime;
    public float spawnDelay;
    public GameObject spawn_2_1intento;
    public GameObject spawn_2_2intento;
    public GameObject spawn_2_3intento;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SpawnObject", spawnTime, spawnDelay);
    }

    // Update is called once per frame
    public void SpawnObject()
    {
        Debug.Log(Random.Range(0, 3));
        int caseSwitch = (Random.Range(0, 3));
        switch (caseSwitch)
        {
            case 0:
                Debug.Log("Case 1");
                Instantiate(spawnee, spawn_2_1intento.transform.position, spawn_2_1intento.transform.rotation);
                if (stopspawning)
                {
                    CancelInvoke("SpawnObject");
                }
                break;
            case 1:
                Debug.Log("Case 2");
                Instantiate(spawnee, spawn_2_2intento.transform.position, spawn_2_2intento.transform.rotation);
                if (stopspawning)
                {
                    CancelInvoke("SpawnObject");
                }
                break;
            case 2:
                Debug.Log("Case 3");
                Instantiate(spawnee, spawn_2_3intento.transform.position, spawn_2_3intento.transform.rotation);
                if (stopspawning)
                {
                    CancelInvoke("SpawnObject");
                }
                break;
                //default:
                // Debug.Log("Default case");
                //break;
        }
    }
}